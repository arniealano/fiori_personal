sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/Fragment",
    "admin/app/studentsapp/model/formatter",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
    function (Controller, JSONModel, Fragment, formatter, Filter, FilterOperator) {
        "use strict";

        return Controller.extend("admin.app.studentsapp.controller.app", {
            formatter: formatter,
            onInit: function () {
                debugger;
                let that = this;
                fetch('https://hanatest-terrific-fossa-je.cfapps.us10.hana.ondemand.com/students').then(res => res.json()).then(data => {
                    // const newData = {...data,
                    //                  results: data.results.map(result => {
                    //                      return{
                    //                          ...result,
                    //                          birthday: that.formatDate(result.birthday) 
                    //                      }
                    //                  })}
                    let oModel = new JSONModel();
                    oModel.setData(data);
                    this.getView().setModel(oModel);
                })
            },
            formatDate: function(dateTime){
                const currDate = new Date();
                const currYear = dateTime.slice(0,4);
                const currMonth =  dateTime.slice(5,7);
                const currDay = dateTime.slice(8,10);
                const currTz = currDate.getTimezoneOffset();
                const offMin = Number(currTz) * -1;
                const zero = '00';
                const newDate = new Date(currYear, currMonth, currDay, zero, zero, zero);
                newDate.setMinutes(newDate.getMinutes() + offMin);
                const newYear = newDate.getFullYear();
                const newMonth = newDate.getMonth();
                const newDay = newDate.getDay();
                const finalDate = `${newYear}-${newMonth}-${newDay}`;
                return finalDate;
            },
            onSearch: function (oEvent) {
                let iQuery = oEvent.getParameter('query');
                const aFilter = [];
                aFilter.push(new Filter("name", FilterOperator.Contains, iQuery));
                let oNewFilter = new Filter({
                    filters: aFilter,
                    and: false
                })
                this.getView().byId('idStudentsTable').getBinding('items').filter(oNewFilter)
            },
            onAdd: function (oEvent) {
                this.action = 'add';
                if (!this.oDialog) {
                    Fragment.load({
                        name: "admin.app.studentsapp.fragments.dialog",
                        controller: this
                    }).then(this._showDialog.bind(this));
                } else {
                    this.oDialog.open()
                }

            },
            onDelete: function (oEvent) {
                let sPath = oEvent.getSource().getBindingContext().getPath();
                const id = this.getView().getModel().getProperty(sPath).id;
                fetch(`https://hanatest-terrific-fossa-je.cfapps.us10.hana.ondemand.com/students/${id}`, {
                    method: 'DELETE'
                }).then(res => res.json()).then(data => {
                    if (data.success === true) {
                        this._rebindTable();
                    }
                })
            },
            onEdit: function (oEvent) {
                this.action = 'edit';
                this.sPath = oEvent.getSource().getBindingContext().getPath();
                if (!this.oDialog) {
                    Fragment.load({
                        name: "admin.app.studentsapp.fragments.dialog",
                        controller: this
                    }).then(this._showEditDialog.bind(this));
                } else {
                    sap.ui.getCore().byId('idSimpleForm').bindElement(this.sPath);
                    this.oDialog.open();
                }
            },
            onSave: function () {
               debugger;
                const fName = sap.ui.getCore().byId('idFname').getValue();
                const mName = sap.ui.getCore().byId('idMname').getValue();
                const lName = sap.ui.getCore().byId('idLname').getValue();
                const oSelected = sap.ui.getCore().byId('idRB').getSelectedButton();
                const gender = oSelected.getText();
                const date = sap.ui.getCore().byId('idDate').getDateValue();
                const address = sap.ui.getCore().byId('idAddress').getValue();
                const contact = sap.ui.getCore().byId('idContact').getValue();
                if (contact.length > 11) {
                    sap.ui.getCore().byId('idContact').setValueState(sap.ui.core.ValueState.Error)
                    sap.ui.getCore().byId('idContact').setValueStateText("Maximum length exceeded!!")
                } else {
                    if (this.action === 'add') {

                        const oParams = {
                            fname: fName,
                            mname: mName,
                            lname: lName,
                            gender: gender,
                            birthday: date.toISOString(),
                            address: address,
                            contactno: contact
                        }

                        fetch('https://hanatest-terrific-fossa-je.cfapps.us10.hana.ondemand.com/students', {
                            method: 'POST',
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(oParams)
                        })
                            .then(res => res.json())
                            .then(data => {
                                if (data.success) {
                                    this._rebindTable();
                                    this.oDialog.close();
                                }
                            })
                    } else {
                        const id = this.getView().getModel().getProperty(this.sPath).id;
                        fetch(`https://hanatest-terrific-fossa-je.cfapps.us10.hana.ondemand.com/students/${id}`, {
                            method: 'PUT',
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify({
                                fname: fName,
                                mname: mName,
                                lname: lName,
                                gender: gender,
                                birthday: date.toISOString(),
                                address: address,
                                contactno: contact
                            })
                        })
                            .then(res => res.json())
                            .then(data => {
                                if (data.success) {
                                    if (this.sPath) {
                                        //bind to none
                                        sap.ui.getCore().byId('idSimpleForm').bindElement('/none');
                                    }
                                    this._rebindTable();
                                    this.oDialog.close();
                                }
                            })
                    }
                }
            },
            onCancel: function () {
                this.oDialog.close();
                if (this.sPath) {
                    //bind to none
                    sap.ui.getCore().byId('idSimpleForm').bindElement('/none');
                }
                sap.ui.getCore().byId('idContact').setValueState(sap.ui.core.ValueState.None)
            },
            _showDialog: function (oFragment) {
                this.oDialog = oFragment;
                this.getView().addDependent(this.oDialog);
                this.oDialog.open()
            },
            _showEditDialog: function (oFragment) {
                this.oDialog = oFragment;
                this.getView().addDependent(this.oDialog);
                sap.ui.getCore().byId('idSimpleForm').bindElement(this.sPath);
                this.oDialog.open()
            },
            _rebindTable: function () {
                debugger;
                let that = this;
                fetch('https://hanatest-terrific-fossa-je.cfapps.us10.hana.ondemand.com/students').then(res => res.json()).then(data => {
                    let oModel = new JSONModel();
                    oModel.setData(data);
                    this.getView().setModel(oModel);
                })
            },
        });
    });
